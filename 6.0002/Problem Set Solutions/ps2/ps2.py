# 6.0002 Problem Set 5
# Graph optimization
# Name:
# Collaborators:
# Time:

#
# Finding shortest paths through MIT buildings
#
import copy
import unittest
from graph import Digraph, Node, WeightedEdge

#
# Problem 2: Building up the Campus Map
#
# Problem 2a: Designing your graph
#
# What do the graph's nodes represent in this problem? What
# do the graph's edges represent? Where are the distances
# represented?
#
# Answer: The nodes of the graph represent the campus buildings
#         The edges represent the directed paths from one building to another
#         The distances are represented as two different weights on each edge:
#               total distance and outdoor distance


# Problem 2b: Implementing load_map
def load_map(map_filename):
    """
    Parses the map file and constructs a directed graph

    Parameters:
        map_filename : name of the map file

    Assumes:
        Each entry in the map file consists of the following four positive
        integers, separated by a blank space:
            From To TotalDistance DistanceOutdoors
        e.g.
            32 76 54 23
        This entry would become an edge from 32 to 76.

    Returns:
        a Digraph representing the map
    """

    print("Loading map from file...")
    campus_map = Digraph() #Create new empty digraph
    map_file = open(map_filename, "r")
    for line in map_file:
        #split line into list of form [src, dest, total, outdoor]
        edge_data = line.split(" ")
        src_building = Node(str(edge_data[0]))
        dest_building = Node(str(edge_data[1]))
        total_dist = int(edge_data[2])
        outdoor_dist = int(edge_data[3])
        new_edge = WeightedEdge(src_building, dest_building, total_dist, outdoor_dist)
        #check for/add each node
        if not campus_map.has_node(src_building):
            campus_map.add_node(src_building)
        if not campus_map.has_node(dest_building):
            campus_map.add_node(dest_building)
        #add edge
        campus_map.add_edge(new_edge)
    return campus_map

# Problem 2c: Testing load_map
# Include the lines used to test load_map below, but comment them out
# print(load_map("test_load_map.txt"))

#
# Problem 3: Finding the Shorest Path using Optimized Search Method
#
# Problem 3a: Objective function (What is to be maximized or minimized)
#
# What is the objective function for this problem? What are the constraints?
#
# Answer: The objective function is minimising the total distance traveled
#         The contraints include that the path must start at the specified
#         start node and end at the specified end, and must contain no more
#         outdoor travel than max_dist_outdoors

# Problem 3b: Implement get_best_path
def get_best_path(digraph, start, end, path, max_dist_outdoors, best_dist,
                  best_path):
    """
    Finds the shortest path between buildings subject to constraints.

    Parameters:
        digraph: Digraph instance
            The graph on which to carry out the search
        start: string
            Building number at which to start
        end: string
            Building number at which to end
        path: list composed of [[list of strings], int, int]
            Represents the current path of nodes being traversed. Contains
            a list of node names, total distance traveled, and total
            distance outdoors.
        max_dist_outdoors: int
            Maximum distance spent outdoors on a path
        best_dist: int
            The smallest distance between the original start and end node
            for the initial problem that you are trying to solve
        best_path: list of strings
            The shortest path found so far between the original start
            and end node.

    Returns:
        A tuple with the shortest-path from start to end, represented by
        a list of building numbers (in strings), [n_1, n_2, ..., n_k],
        where there exists an edge from n_i to n_(i+1) in digraph,
        for all 1 <= i < k and the distance of that path.

        If there exists no path that satisfies max_total_dist and
        max_dist_outdoors constraints, then return None.
    """
    #Make nodes of start and end
    start_node = Node(start)
    end_node = Node(end)
    #Check validity of start and of end
    if not digraph.has_node(start_node):
        raise ValueError("Start node not in graph")
    if not digraph.has_node(end_node):
        raise ValueError("End node not in graph")
    if start_node == end_node:
        return path
    #If valid nodes and not yet at end, make depth first path with each child node not yet in path
    else:
        for edge in digraph.get_edges_for_node(start_node):
            child = edge.get_destination().get_name()
            if child not in path[0] and path[2] + edge.get_outdoor_distance() <= max_dist_outdoors:
                #Make a trial path (deepcopy(!) of path) including new child
                trial_path = copy.deepcopy(path)
                trial_path[0].append(child)
                trial_path[1] += edge.get_total_distance()
                trial_path[2] += edge.get_outdoor_distance()
                #DEBUG: print current dfs path
                #print("Current DFS path:", trial_path[0], "\nLength:", trial_path[1], "\nOutdoors:", trial_path[2])
                trial_result = get_best_path(digraph, child, end, trial_path, max_dist_outdoors, best_dist, best_path)
                #if first valid path found, or better than previous answer, replace old best_path
                if trial_result[0] is not None and (best_path is None or trial_result[1] < best_dist):
                    best_path = trial_result[0].copy()
                    best_dist = trial_result[1]
        return (best_path, best_dist)
                    
    


# Problem 3c: Implement directed_dfs
def directed_dfs(digraph, start, end, max_total_dist, max_dist_outdoors):
    """
    Finds the shortest path from start to end using a directed depth-first
    search. The total distance traveled on the path must not
    exceed max_total_dist, and the distance spent outdoors on this path must
    not exceed max_dist_outdoors.

    Parameters:
        digraph: Digraph instance
            The graph on which to carry out the search
        start: string
            Building number at which to start
        end: string
            Building number at which to end
        max_total_dist: int
            Maximum total distance on a path
        max_dist_outdoors: int
            Maximum distance spent outdoors on a path

    Returns:
        The shortest-path from start to end, represented by
        a list of building numbers (in strings), [n_1, n_2, ..., n_k],
        where there exists an edge from n_i to n_(i+1) in digraph,
        for all 1 <= i < k

        If there exists no path that satisfies max_total_dist and
        max_dist_outdoors constraints, then raises a ValueError.
    """
    (shortest_path, shortest_dist) = get_best_path(digraph, start, end, [[start], 0, 0], max_dist_outdoors, None, None)
    if shortest_path is None or shortest_dist > max_total_dist:
        raise ValueError("No possible path satisfying max distance constraint")
    return shortest_path


# ================================================================
# Begin tests -- you do not need to modify anything below this line
# ================================================================

class Ps2Test(unittest.TestCase):
    LARGE_DIST = 99999

    def setUp(self):
        self.graph = load_map("mit_map.txt")

    def test_load_map_basic(self):
        self.assertTrue(isinstance(self.graph, Digraph))
        self.assertEqual(len(self.graph.nodes), 37)
        all_edges = []
        for _, edges in self.graph.edges.items():
            all_edges += edges  # edges must be dict of node -> list of edges
        all_edges = set(all_edges)
        self.assertEqual(len(all_edges), 129)

    def _print_path_description(self, start, end, total_dist, outdoor_dist):
        constraint = ""
        if outdoor_dist != Ps2Test.LARGE_DIST:
            constraint = "without walking more than {}m outdoors".format(
                outdoor_dist)
        if total_dist != Ps2Test.LARGE_DIST:
            if constraint:
                constraint += ' or {}m total'.format(total_dist)
            else:
                constraint = "without walking more than {}m total".format(
                    total_dist)

        print("------------------------")
        print("Shortest path from Building {} to {} {}".format(
            start, end, constraint))

    def _test_path(self,
                   expectedPath,
                   total_dist=LARGE_DIST,
                   outdoor_dist=LARGE_DIST):
        start, end = expectedPath[0], expectedPath[-1]
        self._print_path_description(start, end, total_dist, outdoor_dist)
        dfsPath = directed_dfs(self.graph, start, end, total_dist, outdoor_dist)
        print("Expected: ", expectedPath)
        print("DFS: ", dfsPath)
        self.assertEqual(expectedPath, dfsPath)

    def _test_impossible_path(self,
                              start,
                              end,
                              total_dist=LARGE_DIST,
                              outdoor_dist=LARGE_DIST):
        self._print_path_description(start, end, total_dist, outdoor_dist)
        with self.assertRaises(ValueError):
            directed_dfs(self.graph, start, end, total_dist, outdoor_dist)

    def test_path_one_step(self):
        self._test_path(expectedPath=['32', '56'])

    def test_path_no_outdoors(self):
        self._test_path(
            expectedPath=['32', '36', '26', '16', '56'], outdoor_dist=0)

    def test_path_multi_step(self):
        self._test_path(expectedPath=['2', '3', '7', '9'])

    def test_path_multi_step_no_outdoors(self):
        self._test_path(
            expectedPath=['2', '4', '10', '13', '9'], outdoor_dist=0)

    def test_path_multi_step2(self):
        self._test_path(expectedPath=['1', '4', '12', '32'])

    def test_path_multi_step_no_outdoors2(self):
        self._test_path(
            expectedPath=['1', '3', '10', '4', '12', '24', '34', '36', '32'],
            outdoor_dist=0)

    def test_impossible_path1(self):
        self._test_impossible_path('8', '50', outdoor_dist=0)

    def test_impossible_path2(self):
        self._test_impossible_path('10', '32', total_dist=100)


if __name__ == "__main__":
    unittest.main()
    # test_graph = load_map("test_load_map.txt")
    # print(test_graph)
    # print(directed_dfs(test_graph,'a', 'c', 100, 100))
    # print(directed_dfs(test_graph,'l', 'o', 100, 100))
    
    # test_graph = load_map("textbook_example_map.txt")
    # print(test_graph)
    # print(directed_dfs(test_graph,'0', '5', 100, 100))
