# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 17:09:59 2020

@author: purpl
"""

from ps3 import * 

#Test 1A:
#3 wide by 3 tall
myRoom = RectangularRoom(3, 3, 23)
print(myRoom.tiles)
assert(myRoom.tiles == [[23,23,23],[23,23,23],[23,23,23]])
try:
    print(myRoom.tiles[2][2])
except:
    assert False
try:
    print(myRoom.tiles[3][3])
    assert False
except:
    assert True

#4 wide by 2 tall
myRoom = RectangularRoom(4, 2, 23)
print(myRoom.tiles)
assert(myRoom.tiles == [[23,23],[23,23],[23,23],[23,23]])
try:
    print(myRoom.tiles[3][1])
except:
    assert False

#2 wide by 4 tall
myRoom = RectangularRoom(2, 4, 23)
print(myRoom.tiles)
assert(myRoom.tiles == [[23,23,23,23],[23,23,23,23]])
try:
    print(myRoom.tiles[1][3])
except:
    assert False

#Test 1B:
myRoom = RectangularRoom(2, 2, 23)
print(myRoom.tiles)
assert(myRoom.tiles == [[23,23],[23,23]])
test_positions = [Position(0.3,0.9), Position(0.1,1.6), Position(1.5,0.2), Position(1.9,1.99999)]
#Test 1F, get_dirt_amount
#Clean first tile by 20
myRoom.clean_tile_at_position(test_positions[0], 20)
assert myRoom.get_dirt_amount(0,0) == 3
#Clean second tile by 40 (set to 0)
myRoom.clean_tile_at_position(test_positions[1], 40)
assert myRoom.get_dirt_amount(0,1) == 0
#Clean third tile by 0
myRoom.clean_tile_at_position(test_positions[2], 0)
assert myRoom.get_dirt_amount(1,0) == 23
#Clean fourth tile by -100
myRoom.clean_tile_at_position(test_positions[3], -100)
assert myRoom.get_dirt_amount(1,1) == 123
print(myRoom.tiles)
assert(myRoom.tiles == [[3,0],[23,123]])
#Test 1C:
assert myRoom.is_tile_cleaned(0,1)
assert not myRoom.is_tile_cleaned(1,1)
#Test 1D
assert myRoom.get_num_cleaned_tiles() == 1
myRoom.clean_tile_at_position(test_positions[1], -10)
print(myRoom.tiles)
assert myRoom.get_num_cleaned_tiles() == 0
for pos in test_positions:
    myRoom.clean_tile_at_position(pos, 1000)
assert myRoom.get_num_cleaned_tiles() == 4
print(myRoom.tiles)
#Test 1E
assert not myRoom.is_position_in_room(Position(20,20))
assert myRoom.is_position_in_room(Position(0,0))
assert myRoom.is_position_in_room(Position(0,1))
assert myRoom.is_position_in_room(Position(1,0))
assert myRoom.is_position_in_room(Position(1,1))
assert not myRoom.is_position_in_room(Position(0,20))
assert not myRoom.is_position_in_room(Position(20,0))
    
#Test 1G
robot_test_room = RectangularRoom(100,100, 23)
#Generate 10 robots and inspect their starting pos and direction
# for robot_num in range(10):
#     robot = Robot(robot_test_room, robot_num, robot_num)
#     print("Robot", robot_num, "Stats:")
#     print("Speed:", robot.speed)
#     print("Capacity:", robot.capacity)
#     print("Position:", robot.position)
#     print("Direction:", robot.direction)
#     print("------------------------------------")

#Test 2A
empty_room_test = EmptyRoom(100,100, 23)
assert empty_room_test.get_num_tiles() == 10000
#Test 2B
assert empty_room_test.is_position_valid(Position(85.987654, 1.23456789))
assert not empty_room_test.is_position_valid(Position(185.987654, 1.23456789))
assert not empty_room_test.is_position_valid(Position(18.987654, -1.23456789))
assert not empty_room_test.is_position_valid(Position(18.987654, -100.23456789))
assert not empty_room_test.is_position_valid(Position(100, 1.23456789))
#Test 2C 
for run in range(10):
    #Examine 10 random positions (Note: Poisitons' __str__ rounds down)
    print(empty_room_test.get_random_position())
#Test 2D
furnished_room_test = FurnishedRoom(20, 20, 23)
assert furnished_room_test.furniture_tiles == []
#Test 2E (before furniture)
assert furnished_room_test.is_position_valid(Position(15.987654, 1.23456789))
assert not furnished_room_test.is_position_valid(Position(185.987654, 1.23456789))
assert not furnished_room_test.is_position_valid(Position(18.987654, -1.23456789))
assert not furnished_room_test.is_position_valid(Position(18.987654, -100.23456789))
assert not furnished_room_test.is_position_valid(Position(20, 1.23456789))
#Test 2F before furniture
assert furnished_room_test.get_num_tiles() == 400
furnished_room_test.add_furniture_to_room()
assert not furnished_room_test.furniture_tiles == []
furniture = furnished_room_test.furniture_tiles
#print(furniture)
for width in range(20):
    for height in range(20):
        if (width,height) in furniture:
            assert furnished_room_test.is_tile_furnished(width, height)
            assert furnished_room_test.is_position_furnished(Position(width, height))
        else:
            assert not furnished_room_test.is_tile_furnished(width, height)
            assert not furnished_room_test.is_position_furnished(Position(width, height))
#Test 2E (after furniture)
assert not furnished_room_test.is_position_valid(Position(furniture[0][0], furniture[0][1]))
#Test 2F after furniture
assert furnished_room_test.get_num_tiles() == 400 - len(furniture)
#Test 2G
for run in range(10):
    #Examine 10 random positions (Note: Poisitons' __str__ rounds down)
    print(furnished_room_test.get_random_position())






