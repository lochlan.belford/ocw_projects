###########################
# 6.0002 Problem Set 1a: Space Cows 
# Name:
# Collaborators:
# Time:

from ps1_partition import get_partitions
import time

#================================
# Part A: Transporting Space Cows
#================================

# Problem 1
def load_cows(filename):
    """
    Read the contents of the given file.  Assumes the file contents contain
    data in the form of comma-separated cow name, weight pairs, and return a
    dictionary containing cow names as keys and corresponding weights as values.

    Parameters:
    filename - the name of the data file as a string
    
    Assumptions about input:
    file contains only lines of form "<cow_name:str>,<cow_weight:int>"
    all cow names are unique

    Returns:
    a dictionary of cow name (string), weight (int) pairs
    """
    cows = {} #a dictionary containing the names and weights of all cows in file
    cow_file = open(filename, 'r')
    for line in cow_file:
        #creates a 2 element list [<cow_name>, <weight>]
        cow_data = line.split(",")
        cows[str(cow_data[0])] = int(cow_data[1])
    cow_file.close()
    return cows

# Problem 2
def greedy_cow_transport(cows,limit=10):
    """
    Uses a greedy heuristic to determine an allocation of cows that attempts to
    minimize the number of spaceship trips needed to transport all the cows. The
    returned allocation of cows may or may not be optimal.
    The greedy heuristic should follow the following method:

    1. As long as the current trip can fit another cow, add the largest cow that 
        will fit to the trip
    2. Once the trip is full, begin a new trip to transport the remaining cows

    Does not mutate the given dictionary of cows.

    Parameters:
    cows - a dictionary of name (string), weight (int) pairs
    limit - weight limit of the spaceship (an int)
    
    Returns:
    A list of lists, with each inner list containing the names of cows
    transported on a particular trip and the overall list containing all the
    trips
    """
    
    def greedy_run(cows, limit):
        """
        Helper function
        Performs one greedy selection of cows and returns a list of the names
        of the chosen cows

        Parameters
        ----------
        cows : dictionary
        limit : int
        Returns
        -------
        A list of names of the heaviest individual cows that can be selected
        under the provided limit

        """
        #Construct list to hold names of chosen cows, track total weight
        cows_chosen = []
        current_weight = 0
        #pre_defined index names
        name = 0
        weight = 1
        #Get COPY list of cow dictionary items sorted by weight, descending
        #List of form [(<cow_name>,<weight>), (<cow_name>,<weight>), ...]
        remaining_cows = sorted(cows.items(), key=lambda x: x[weight], reverse=True)
        #Iterate over, recording names of selected cows
        for cow in remaining_cows:
            if current_weight + cow[weight] <= limit:
                current_weight += cow[weight]
                cows_chosen.append(cow[name])
            #If totally full, end trip here
            if current_weight == limit:
                break
        return cows_chosen
    
    #Create empty list for storing all trips
    greedy_trips = []
    cows_copy = cows.copy()
    next_trip = greedy_run(cows_copy, limit)
    #End loop when no more cows or if empty list is ever returned
    while len(cows_copy) > 0 and len(next_trip) > 0:
        greedy_trips.append(next_trip)
        #Takes advantage of fact that names are all unique
        for name in next_trip:
            del cows_copy[name]
        next_trip = greedy_run(cows_copy, limit)
    #Return list of all trips
    return greedy_trips

# Problem 3
def brute_force_cow_transport(cows,limit=10):
    """
    Finds the allocation of cows that minimizes the number of spaceship trips
    via brute force.  The brute force algorithm should follow the following 
    method:

    1. Enumerate all possible ways that the cows can be divided into separate 
        trips. Use the given get_partitions function in ps1_partition.py to 
        help you!
    2. Select the allocation that minimizes the number of trips without making 
        any trip that does not obey the weight limitation
            
    Does not mutate the given dictionary of cows.

    Parameters:
    cows - a dictionary of name (string), weight (int) pairs
    limit - weight limit of the spaceship (an int)
    
    Returns:
    A list of lists, with each inner list containing the names of cows
    transported on a particular trip and the overall list containing all the
    trips
    """  
    for partition in get_partitions(cows):
        #Assume the partition is made only of valid trips
        valid_partition = True
        #Then, go through each trip in the set, and check
        for trip in partition:
            #Add up the weights associated with each name
            trip_weight = 0
            for name in trip:
                trip_weight += cows[name]
                #Stop adding as soon as limit broken, mark invalid
                if trip_weight > limit:
                    valid_partition = False
                    break
            #As soon as one trip breaks validity, stop testing this partition
            if not valid_partition:
                break
        #If whole partition was valid, return this as solution
        if valid_partition:
            return partition
    return None
                
                
            
        
# Problem 4
def compare_cow_transport_algorithms():
    """
    Using the data from ps1_cow_data.txt and the specified weight limit, run your
    greedy_cow_transport and brute_force_cow_transport functions here. Use the
    default weight limits of 10 for both greedy_cow_transport and
    brute_force_cow_transport.
    
    Print out the number of trips returned by each method, and how long each
    method takes to run in seconds.

    Returns:
    Does not return anything.
    """
    cows = load_cows("ps1_cow_data.txt")
    #Time Greedy
    start_g = time.time()
    greedy_result = greedy_cow_transport(cows)
    end_g = time.time()
    print("Minimum Trips (greedy algorithm):", len(greedy_result))
    print("Time Taken (greedy algorithm):", end_g - start_g)
    #Time Brute Force
    start_b = time.time()
    brute_result = brute_force_cow_transport(cows)
    end_b = time.time()
    print("Minimum Trips (brute algorithm):", len(brute_result))
    print("Time Taken (brute algorithm):", end_b - start_b)

if __name__ == "__main__":
    #Test Data
    cows = load_cows("ps1_cow_data.txt")
    cows_original = cows.copy()
    cows2 = load_cows("ps1_cow_data_2.txt")
    cows2_original = cows2.copy()
    cows3 = load_cows("my_test_cow_data_2.txt")
    cows3_original = cows3.copy()
    
    assert(cows == cows_original)
    assert(cows2 == cows2_original)
    assert(cows3 == cows3_original)
    
    compare_cow_transport_algorithms()
    
    assert(cows == cows_original)
    assert(cows2 == cows2_original)
    assert(cows3 == cows3_original)
        
    
#Part 5: Writeup
"""
1. The greedy algorithm always runs faster than the brute force algorithm.
    Even in the worst case, each individual greedy_run is 0(nlogn) (from the
    sorting process), and it might run up to n times if only one cow can be
    chosen each time, resulting in 0(n) * 0(nlogn) = 0(n**2 * log(n)).
    The Brute Force algorthim on the other hand always begins by generating
    all possible partitions of the list of n cows. This is an inherently
    exponential process, and so in the worst case, the brute force approach
    runs 0(2**n). This is why the greedy algortihm still finishes nearly
    instantaneously on a list of 26 cows, whereas brute force already runs
    far too long to be useful on a list of that size.

(Still struggling here, I cheated an looked up someone elses answer.
I would have said always correct in this case, can't think of 
case where it fails)
2. In this problem, the greedy algorithm usually does return the optimal solution,
in contrast to the single pass 0/1 knapsack problem, where it often does not. The
added property of returning to the dataset until everything has been chosen
results in a very usable answer to the optimization problem. However, there is
still no guarentee that it will ALWAYS return the optimal solution. Wish I could
prove that.

3. The Brute force algorithm DOES always return an optimal solution, since
it enumerates all possible solutions in length order, returning once it finds
the first valid solution, which is therefore guarenteed to be the shortest possible.
However, the time complexity of the algorithm prevents it from being a practical
solution, despite its guarenteed accuracy.
"""    


