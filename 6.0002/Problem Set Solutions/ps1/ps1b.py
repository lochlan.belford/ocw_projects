###########################
# 6.0002 Problem Set 1b: Space Change
# Name:
# Collaborators:
# Time:
# Author: charz, cdenise

#================================
# Part B: Golden Eggs
#================================

# Problem 1
def dp_make_weight(egg_weights, target_weight, memo = {}):
    """
    Find number of eggs to bring back, using the smallest number of eggs. Assumes there is
    an infinite supply of eggs of each weight, and there is always a egg of value 1.
    
    Parameters:
    egg_weights - tuple of integers, available egg weights sorted from smallest to largest value (1 = d1 < d2 < ... < dk)
    target_weight - int, amount of weight we want to find eggs to fit
    memo - dictionary, OPTIONAL parameter for memoization (you may not need to use this parameter depending on your implementation)
    
    Returns: int, smallest number of eggs needed to make target weight
    """
    #Memoization: Try to retrieve optimal solution for current weight from memo
    # try:
    #     return memo[target_weight]
    # except KeyError:
    #     #If not in memo, solve for current weight
    #     #If there is enough weight left to take more than one type of egg
    #     #explore each possible branch
    #     #Exploit fact that egg weights are sorted
    #     print(memo)
    #     print("Solving for target=" + str(target_weight))
    #     if target_weight >= egg_weights[1]:
    #         least_possible = None
    #         for weight in egg_weights:
    #             #choose that egg, subtract from target weight
    #             choice_result = 1 + dp_make_weight(egg_weights, target_weight - weight, memo)
    #             #check if new result is better than best try so far, or is first try
    #             if least_possible is None or \
    #                 (not choice_result is None and choice_result < least_possible):
    #                 least_possible = choice_result
    #         #Once you test all choices, record result in memo
    #         memo[target_weight] = least_possible
    #     #If there is only one possible choice left, but it won't work,
    #     # i.e. target weight = 3, smallest weight = 2 return None (?) 
    #     #[This is my own dubious modification for hopeful completeness]
    #     elif not (target_weight % egg_weights[0] == 0):
    #         #record dead end
    #         memo[target_weight] = None
    #     else:
    #         memo[target_weight] = target_weight//egg_weights[0]
    #     #Memo is definitely set by now, return
    #     return memo[target_weight]
    
    #Tabulation
    #Build table of best choice at all possible weights
    #Assume for now that 1 is always included as an option
    result_table = {0: 0}
    for remaining_weight in range(1, target_weight+1):
        #explore all possible remaining options
        fewest_eggs = target_weight
        for weight in egg_weights:
            if weight > remaining_weight:
                break
            #Pick one egg of "weight", then add previously solved subproblem
            egg_count = 1
            current_weight = remaining_weight - weight
            egg_count += result_table[current_weight]
            if egg_count < fewest_eggs:
                fewest_eggs = egg_count
        #Found smallest answer for remaining_weight, record in table
        result_table[remaining_weight] = fewest_eggs
        #print("Least eggs for", remaining_weight, "lbs available is:", fewest_eggs)
    return result_table[target_weight]

def eggs_greedy(egg_weights, target_weight):
    desc_weights = sorted(egg_weights, reverse = True)
    current_weight = 0
    index = 0
    egg_count = 0
    while current_weight < target_weight:
        try:
            while current_weight + desc_weights[index] > target_weight:
                index += 1
            current_weight += desc_weights[index]
            egg_count += 1
        except IndexError:
            break
    print(current_weight)
    return egg_count
        

# EXAMPLE TESTING CODE, feel free to add more if you'd like
if __name__ == '__main__':
    egg_weights = (1, 5, 10, 25)
    n = 99
    print("Egg weights = (1, 5, 10, 25)")
    print("n = 99")
    print("Expected ouput: 9 (3 * 25 + 2 * 10 + 4 * 1 = 99)")
    print("Actual output:", dp_make_weight(egg_weights, n))
    print("Greedy output:", eggs_greedy(egg_weights, n))
    print()
    
    egg_weights = (1, 5, 10, 25)
    n = 6
    print("Egg weights = (1, 5, 10, 25)")
    print("n = 6")
    print("Expected ouput: 2 (1 * 5 + 1 * 1 = 6)")
    print("Actual output:", dp_make_weight(egg_weights, n))
    print("Greedy output:", eggs_greedy(egg_weights, n))
    print()
    
    egg_weights = (2, 3, 5, 10, 11, 23, 25, 27, 30, 32, 34, 35, 41, 55, 57, 58, 60, 62, 66, 71, 75, 76, 79, 80, 84, 87, 91, 93, 99, 101)
    n = 268
    print("Egg weights = (2, 3, 5, 10, 11, 23, 25, 27, 30, 32, 34, 35, 41, 55, 57, 58, 60, 62, 66, 71, 75, 76, 79, 80, 84, 87, 91, 93, 99, 101)")
    print("n = 1001")
    print("Expected ouput: 11 (9 * 101 + 1 * 87 + 1 * 5 = 1001)")
    print("Actual output:", dp_make_weight(egg_weights, n))
    print("Greedy output:", eggs_greedy(egg_weights, n))
    print()
    
    egg_weights = (1, 2, 3, 5, 10, 11, 23, 25, 27, 30, 32, 34, 35, 41, 55, 57, 58, 60, 62, 66, 71, 75, 76, 79, 80, 84, 87, 91, 93, 99, 101)
    n = 1001
    print("Egg weights = (2, 3, 5, 10, 11, 23, 25, 27, 30, 32, 34, 35, 41, 55, 57, 58, 60, 62, 66, 71, 75, 76, 79, 80, 84, 87, 91, 93, 99, 101)")
    print("n = 1001")
    print("Expected ouput: 11 (9 * 101 + 1 * 87 + 1 * 5 = 1001)")
    print("Actual output:", dp_make_weight(egg_weights, n))
    print("Greedy output:", eggs_greedy(egg_weights, n))
    print()
    
    egg_weights = (1, 2, 3, 5, 10, 11, 23, 25, 27, 30, 32, 34, 35, 41, 55, 57, 58, 60, 62, 66, 71, 75, 76, 79, 80, 84, 87, 91, 93, 99, 101)
    n = 1002
    print("Egg weights = (2, 3, 5, 10, 11, 23, 25, 27, 30, 32, 34, 35, 41, 55, 57, 58, 60, 62, 66, 71, 75, 76, 79, 80, 84, 87, 91, 93, 99, 101)")
    print("n = 1002")
    # print("Expected ouput: 11 (9 * 101 + 1 * 87 + 1 * 5 = 1001)")
    print("Actual output:", dp_make_weight(egg_weights, n))
    print("Greedy output:", eggs_greedy(egg_weights, n))
    print()
    
    egg_weights = (1, 2, 3, 5, 10, 11, 23, 25, 27, 30, 32, 34, 35, 41, 55, 57, 58, 60, 62, 66, 71, 75, 76, 79, 80, 84, 87, 91, 93, 99, 101)
    n = 323467
    print("Egg weights = (2, 3, 5, 10, 11, 23, 25, 27, 30, 32, 34, 35, 41, 55, 57, 58, 60, 62, 66, 71, 75, 76, 79, 80, 84, 87, 91, 93, 99, 101)")
    print("n = 323467")
    # print("Expected ouput: 11 (9 * 101 + 1 * 87 + 1 * 5 = 1001)")
    print("Actual output:", dp_make_weight(egg_weights, n))
    print("Greedy output:", eggs_greedy(egg_weights, n))
    print()
    
    egg_weights = (1, 2, 3, 5, 10, 11, 23, 25, 27, 30, 32, 34, 35, 41, 55, 57, 58, 60, 62, 66, 71, 75, 76, 79, 80, 84, 87, 91, 93, 99, 101)
    n = 323468
    print("Egg weights = (2, 3, 5, 10, 11, 23, 25, 27, 30, 32, 34, 35, 41, 55, 57, 58, 60, 62, 66, 71, 75, 76, 79, 80, 84, 87, 91, 93, 99, 101)")
    print("n = 323468")
    # print("Expected ouput: 11 (9 * 101 + 1 * 87 + 1 * 5 = 1001)")
    print("Actual output:", dp_make_weight(egg_weights, n))
    print("Greedy output:", eggs_greedy(egg_weights, n))
    print()
    
    # egg_weights = (1, 2, 3, 5, 10, 11, 23, 25, 27, 30, 32, 34, 35, 41, 55, 57, 58, 60, 62, 66, 71, 75, 76, 79, 80, 84, 87, 91, 93, 99, 101)
    # n = 8768900
    # print("Egg weights = (2, 3, 5, 10, 11, 23, 25, 27, 30, 32, 34, 35, 41, 55, 57, 58, 60, 62, 66, 71, 75, 76, 79, 80, 84, 87, 91, 93, 99, 101)")
    # print("n = 8768900")
    # # print("Expected ouput: 11 (9 * 101 + 1 * 87 + 1 * 5 = 1001)")
    # print("Actual output:", dp_make_weight(egg_weights, n))
    # print("Greedy output:", eggs_greedy(egg_weights, n))
    # print()
    
    egg_weights = (1, 52, 99, 100)
    n = 404
    print("Egg weights = (1, 52, 99, 100)")
    print("n = 404")
    print("Expected DP ouput: 5 (3 * 10 + 2 * 52 = 404)")
    print("Expected Greedy ouput: 8 (4 * 100 + 4 * 1 = 404)")
    print("Actual output:", dp_make_weight(egg_weights, n))
    print("Greedy output:", eggs_greedy(egg_weights, n))
    
"""
Write up and self-evaluation:
1. Brute forcing this problem while len(egg_weights) == 30 would involve building
a decision tree with 30 branches at each level. With the number of levels bounded
by n (the target weight), this means the complexity of the problem is O(30**n)!
2. A Greedy algorithm would pick the highest weight egg that still fit in the
ship each time, but depending on the available weight, it might find itself in
a dead end. Even more often, its solution would be close, since most solutions
involve using as many as possible of the heaviest weight, but not quite as short
as the true most efficient set, which likely skips the next highest weight for
something with more appropriateness
3. See final test case above for example of when greedy fails to be optimal!

Self-evaluation: I'm in the tougher stuff now--feel like I am 90% of the way there
on Dynamic Programming. Might should come back and try top-down. Proud of how
much I worked today though!
"""
    
    