# -*- coding: utf-8 -*-
# Problem Set 5: Experimental Analysis
# Name: 
# Collaborators (discussion):
# Time:

import pylab
import re

# cities in our weather data
CITIES = [
    'BOSTON',
    'SEATTLE',
    'SAN DIEGO',
    'PHILADELPHIA',
    'PHOENIX',
    'LAS VEGAS',
    'CHARLOTTE',
    'DALLAS',
    'BALTIMORE',
    'SAN JUAN',
    'LOS ANGELES',
    'MIAMI',
    'NEW ORLEANS',
    'ALBUQUERQUE',
    'PORTLAND',
    'SAN FRANCISCO',
    'TAMPA',
    'NEW YORK',
    'DETROIT',
    'ST LOUIS',
    'CHICAGO'
]

TRAINING_INTERVAL = range(1961, 2010)
TESTING_INTERVAL = range(2010, 2016)

"""
Begin helper code
"""
class Climate(object):
    """
    The collection of temperature records loaded from given csv file
    """
    def __init__(self, filename):
        """
        Initialize a Climate instance, which stores the temperature records
        loaded from a given csv file specified by filename.

        Args:
            filename: name of the csv file (str)
        """
        self.rawdata = {}

        f = open(filename, 'r')
        header = f.readline().strip().split(',')
        for line in f:
            items = line.strip().split(',')

            date = re.match('(\d\d\d\d)(\d\d)(\d\d)', items[header.index('DATE')])
            year = int(date.group(1))
            month = int(date.group(2))
            day = int(date.group(3))

            city = items[header.index('CITY')]
            temperature = float(items[header.index('TEMP')])
            if city not in self.rawdata:
                self.rawdata[city] = {}
            if year not in self.rawdata[city]:
                self.rawdata[city][year] = {}
            if month not in self.rawdata[city][year]:
                self.rawdata[city][year][month] = {}
            self.rawdata[city][year][month][day] = temperature
            
        f.close()

    def get_yearly_temp(self, city, year):
        """
        Get the daily temperatures for the given year and city.

        Args:
            city: city name (str)
            year: the year to get the data for (int)

        Returns:
            a 1-d pylab array of daily temperatures for the specified year and
            city
        """
        temperatures = []
        assert city in self.rawdata, "provided city is not available"
        assert year in self.rawdata[city], "provided year is not available"
        for month in range(1, 13):
            for day in range(1, 32):
                if day in self.rawdata[city][year][month]:
                    temperatures.append(self.rawdata[city][year][month][day])
        return pylab.array(temperatures)

    def get_daily_temp(self, city, month, day, year):
        """
        Get the daily temperature for the given city and time (year + date).

        Args:
            city: city name (str)
            month: the month to get the data for (int, where January = 1,
                December = 12)
            day: the day to get the data for (int, where 1st day of month = 1)
            year: the year to get the data for (int)

        Returns:
            a float of the daily temperature for the specified time (year +
            date) and city
        """
        assert city in self.rawdata, "provided city is not available"
        assert year in self.rawdata[city], "provided year is not available"
        assert month in self.rawdata[city][year], "provided month is not available"
        assert day in self.rawdata[city][year][month], "provided day is not available"
        return self.rawdata[city][year][month][day]

def se_over_slope(x, y, estimated, model):
    """
    For a linear regression model, calculate the ratio of the standard error of
    this fitted curve's slope to the slope. The larger the absolute value of
    this ratio is, the more likely we have the upward/downward trend in this
    fitted curve by chance.
    
    Args:
        x: an 1-d pylab array with length N, representing the x-coordinates of
            the N sample points
        y: an 1-d pylab array with length N, representing the y-coordinates of
            the N sample points
        estimated: an 1-d pylab array of values estimated by a linear
            regression model
        model: a pylab array storing the coefficients of a linear regression
            model

    Returns:
        a float for the ratio of standard error of slope to slope
    """
    assert len(y) == len(estimated)
    assert len(x) == len(estimated)
    EE = ((estimated - y)**2).sum()
    var_x = ((x - x.mean())**2).sum()
    SE = pylab.sqrt(EE/(len(x)-2)/var_x)
    return SE/model[0]

"""
End helper code
"""

def generate_models(x, y, degs):
    """
    Generate regression models by fitting a polynomial for each degree in degs
    to points (x, y).

    Args:
        x: an 1-d pylab array with length N, representing the x-coordinates of
            the N sample points
        y: an 1-d pylab array with length N, representing the y-coordinates of
            the N sample points
        degs: a list of degrees of the fitting polynomial

    Returns:
        a list of pylab arrays, where each array is a 1-d array of coefficients
        that minimizes the squared error of the fitting polynomial
    """
    models = []
    for d in degs:
        models.append(pylab.polyfit(x,y,d))
    return models

#Test problem A1
#print(generate_models(pylab.array([1961,1962,1963]), pylab.array([-4.4,-5.5,-6.6]), [1,2]))

def r_squared(y, estimated):
    """
    Calculate the R-squared error term.
    
    Args:
        y: 1-d pylab array with length N, representing the y-coordinates of the
            N sample points
        estimated: an 1-d pylab array of values estimated by the regression
            model

    Returns:
        a float for the R-squared error term
    """
    mean = pylab.mean(y)
    numerator = sum((y-estimated)**2)
    denominator = sum((y-mean)**2)
    return 1 - (numerator/denominator)

def evaluate_models_on_training(x, y, models):
    """
    For each regression model, compute the R-squared value for this model with the
    standard error over slope of a linear regression line (only if the model is
    linear), and plot the data along with the best fit curve.

    For the plots, you should plot data points (x,y) as blue dots and your best
    fit curve (aka model) as a red solid line. You should also label the axes
    of this figure appropriately and have a title reporting the following
    information:
        degree of your regression model,
        R-square of your model evaluated on the given data points,
        and SE/slope (if degree of this model is 1 -- see se_over_slope). 

    Args:
        x: an 1-d pylab array with length N, representing the x-coordinates of
            the N sample points
        y: an 1-d pylab array with length N, representing the y-coordinates of
            the N sample points
        models: a list containing the regression models you want to apply to
            your data. Each model is a pylab array storing the coefficients of
            a polynomial.

    Returns:
        None
    """
    figure_count = 0
    for model in models:
        #Calculate
        estimated_vals = []
        r_sq = None
        se_slope = None
        for x_val in x:
            est_y = 0
            for degree in range(len(model)):
                #print(degree)
                #print(model[degree], "*", x_val,"^", len(model) - 1 - degree,)
                est_y += model[degree] * (x_val**(len(model) - 1 - degree))
            #print(est_y)
            estimated_vals.append(est_y)
        estimated = pylab.array(estimated_vals)
        r_sq = r_squared(y, estimated)
        #For linear fits, calc SE/slope as well
        if len(model) <= 2:
            se_slope = se_over_slope(x, y, estimated, model)
        #Plot
        figure_count += 1
        pylab.figure(figure_count)
        title = "Degree of Fit: " + str(len(model) - 1) + "\nR^2: " + str(r_sq)
        if se_slope is not None:
            title += "\nSE/Slope: " + str(se_slope)
        pylab.title(title)
        pylab.xlabel("Years")
        pylab.ylabel("Deg. Celsius")
        pylab.plot(x, y, "bo")
        pylab.plot(x, estimated, "r-")
        
        

def gen_cities_avg(climate, multi_cities, years):
    """
    Compute the average annual temperature over multiple cities.

    Args:
        climate: instance of Climate
        multi_cities: the names of cities we want to average over (list of str)
        years: the range of years of the yearly averaged temperature (list of
            int)

    Returns:
        a pylab 1-d array of floats with length = len(years). Each element in
        this array corresponds to the average annual temperature over the given
        cities for a given year.
    """
    national_avg_per_year = []
    for year in years:
        city_avgs = []    
        for city in multi_cities:
            temps_for_year = pylab.array(climate.get_yearly_temp(city, year))
            city_avgs.append(pylab.mean(temps_for_year))
        national_avg_per_year.append(pylab.mean(pylab.array(city_avgs)))
    return pylab.array(national_avg_per_year)

def moving_average(y, window_length):
    """
    Compute the moving average of y with specified window length.

    Args:
        y: an 1-d pylab array with length N, representing the y-coordinates of
            the N sample points
        window_length: an integer indicating the window length for computing
            moving average

    Returns:
        an 1-d pylab array with the same length as y storing moving average of
        y-coordinates of the N sample points
    """
    moving_avgs = []
    for index in range(len(y)):
        if index < (window_length - 1):
            moving_avgs.append(sum(y[:index+1])/(index + 1))
        else:
            moving_avgs.append(sum(y[index-window_length+1:index+1])/window_length)
    return moving_avgs

#print(moving_average(pylab.array([10,20,30,40,50]),3))

def rmse(y, estimated):
    """
    Calculate the root mean square error term.

    Args:
        y: an 1-d pylab array with length N, representing the y-coordinates of
            the N sample points
        estimated: an 1-d pylab array of values estimated by the regression
            model

    Returns:
        a float for the root mean square error term
    """
    numerator = sum((y - estimated)**2)
    denominator = len(y)
    return (numerator/denominator) ** 0.5

def gen_std_devs(climate, multi_cities, years):
    """
    For each year in years, compute the standard deviation over the averaged yearly
    temperatures for each city in multi_cities. 

    Args:
        climate: instance of Climate
        multi_cities: the names of cities we want to use in our std dev calculation (list of str)
        years: the range of years to calculate standard deviation for (list of int)

    Returns:
        a pylab 1-d array of floats with length = len(years). Each element in
        this array corresponds to the standard deviation of the average annual 
        city temperatures for the given cities in a given year.
    """
    national_stdv_per_year = []
    for year in years:
        daily_avgs = []
        for month in range(1, 13):
            if month in (1, 3, 5, 7, 8, 10, 12):
                num_days = 31
            elif month == 2:
                if year % 4 == 0:
                    num_days = 29
                else:
                    num_days = 28
            else:
                num_days = 30
            for day in range(1, num_days + 1):
                city_temps_on_day = []
                for city in multi_cities:
                    city_temps_on_day.append(climate.get_daily_temp(city, month, day, year))
                daily_avgs.append(pylab.mean(pylab.array(city_temps_on_day)))
        national_stdv_per_year.append(pylab.std(pylab.array(daily_avgs)))
    return pylab.array(national_stdv_per_year)
            

def evaluate_models_on_testing(x, y, models):
    """
    For each regression model, compute the RMSE for this model and plot the
    test data along with the model’s estimation.

    For the plots, you should plot data points (x,y) as blue dots and your best
    fit curve (aka model) as a red solid line. You should also label the axes
    of this figure appropriately and have a title reporting the following
    information:
        degree of your regression model,
        RMSE of your model evaluated on the given data points. 

    Args:
        x: an 1-d pylab array with length N, representing the x-coordinates of
            the N sample points
        y: an 1-d pylab array with length N, representing the y-coordinates of
            the N sample points
        models: a list containing the regression models you want to apply to
            your data. Each model is a pylab array storing the coefficients of
            a polynomial.

    Returns:
        None
    """
    figure_count = 10000
    for model in models:
        #Calculate
        estimated_vals = []
        trial_rmse = None
        for x_val in x:
            est_y = 0
            for degree in range(len(model)):
                #print(degree)
                #print(model[degree], "*", x_val,"^", len(model) - 1 - degree,)
                est_y += model[degree] * (x_val**(len(model) - 1 - degree))
            #print(est_y)
            estimated_vals.append(est_y)
        estimated = pylab.array(estimated_vals)
        trial_rmse = rmse(y, estimated)
        #Plot
        figure_count += 1
        pylab.figure(figure_count)
        title = "Degree of Fit: " + str(len(model) - 1) + "\nRMSE: " + str(trial_rmse)
        pylab.title(title)
        pylab.xlabel("Years")
        pylab.ylabel("Deg. Celsius")
        pylab.plot(x, y, "bo")
        pylab.plot(x, estimated, "r-")

if __name__ == '__main__':

    climate_data = Climate("data.csv")

    # Part A.4
    # x = pylab.array(TRAINING_INTERVAL)
    # jan_10_samples = []
    # for year in TRAINING_INTERVAL:
    #     jan_10_temp = climate_data.get_daily_temp("NEW YORK", 1, 10, year)
    #     # print(jan_10_temp)
    #     jan_10_samples.append(jan_10_temp)
    # y = pylab.array(jan_10_samples)
    # # print(x)
    # # print(y)
    # models = generate_models(x, y, [1])
    # #print(models)
    # evaluate_models_on_training(x, y, models)
    
    # Part A.4.II
    # x = pylab.array(TRAINING_INTERVAL)
    # yearly_avgs = []
    # for year in TRAINING_INTERVAL:
    #     year_of_temps = climate_data.get_yearly_temp("NEW YORK", year)
    #     yearly_avgs.append(pylab.mean(pylab.array(year_of_temps)))
    # y = pylab.array(yearly_avgs)
    # print(x)
    # print(y)
    # models = generate_models(x, y, [1])
    # evaluate_models_on_training(x,y,models)    
    
    # Part B
    # x = pylab.array(TRAINING_INTERVAL)
    # y = gen_cities_avg(climate_data, CITIES, TRAINING_INTERVAL)
    # print(x)
    # print(y)
    # models = generate_models(x,y,[1])
    # evaluate_models_on_training(x,y,models)

    # Part C
    # training_x = pylab.array(TRAINING_INTERVAL)
    # training_y = moving_average(gen_cities_avg(climate_data, CITIES, TRAINING_INTERVAL), 5)
    # print(training_x)
    # print(training_y)
    # models = generate_models(training_x, training_y,[1])
    # evaluate_models_on_training(training_x, training_y, models)

    # Part D
    # training_x = pylab.array(TRAINING_INTERVAL)
    # training_y = moving_average(gen_cities_avg(climate_data, CITIES, TRAINING_INTERVAL), 5)
    # print(training_x)
    # print(training_y)
    # models = generate_models(training_x, training_y,[1, 2])
    # evaluate_models_on_training(training_x, training_y, models)
    # test_x = pylab.array(TESTING_INTERVAL)
    # test_y = moving_average(gen_cities_avg(climate_data, CITIES, TESTING_INTERVAL), 5)
    # print(test_x)
    # print(test_y)
    # evaluate_models_on_testing(test_x, test_y, models)

    # Part E
    training_x = pylab.array(TRAINING_INTERVAL)
    training_y = moving_average(gen_std_devs(climate_data, CITIES, TRAINING_INTERVAL), 5)
    print(training_x)
    print(training_y)
    models = generate_models(training_x, training_y,[1])
    evaluate_models_on_training(training_x, training_y, models)
