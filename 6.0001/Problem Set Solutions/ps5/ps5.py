# 6.0001/6.00 Problem Set 5 - RSS Feed Filter
# Name:
# Collaborators:
# Time:

import feedparser
import string
import time
import threading
from project_util import translate_html
from mtTkinter import *
from datetime import datetime
import pytz


#-----------------------------------------------------------------------

#======================
# Code for retrieving and parsing
# Google and Yahoo News feeds
# Do not change this code
#======================

def process(url):
    """
    Fetches news items from the rss url and parses them.
    Returns a list of NewsStory-s.
    """
    feed = feedparser.parse(url)
    entries = feed.entries
    ret = []
    for entry in entries:
        guid = entry.guid
        title = translate_html(entry.title)
        link = entry.link
        description = translate_html(entry.description)
        pubdate = translate_html(entry.published)

        try:
            pubdate = datetime.strptime(pubdate, "%a, %d %b %Y %H:%M:%S %Z")
            pubdate.replace(tzinfo=pytz.timezone("GMT"))
          #  pubdate = pubdate.astimezone(pytz.timezone('EST'))
          #  pubdate.replace(tzinfo=None)
        except ValueError:
            pubdate = datetime.strptime(pubdate, "%a, %d %b %Y %H:%M:%S %z")

        newsStory = NewsStory(guid, title, description, link, pubdate)
        ret.append(newsStory)
    return ret

#======================
# Data structure design
#======================

# Problem 1

# TODO: NewsStory
class NewsStory(object):
    """
    NewsStory objects represent a single news article from a news RSS feed
    The constructor takes in 5 arguments:
        guid: string, globally unique identifier
        title: string, article title
        description: string, summary of the article
        link: string: url link to the web version of the newsfeed
        pubdate: datetime, date of publication
    
    The class contains 5 getter methods, one for each of the above attributes
    """
    def __init__(self, guid, title, description, link, pubdate):
        self.guid = guid
        self.title = title
        self.description = description
        self.link = link
        self.pubdate = pubdate
    
    def get_guid(self):
        return self.guid
    
    def get_title(self):
        return self.title
    
    def get_description(self):
        return self.description
    
    def get_link(self):
        return self.link
    
    def get_pubdate(self):
        return self.pubdate
    
#======================
# Triggers
#======================

class Trigger(object):
    def evaluate(self, story):
        """
        Returns True if an alert should be generated
        for the given news item, or False otherwise.
        """
        # DO NOT CHANGE THIS!
        raise NotImplementedError

# PHRASE TRIGGERS

# Problem 2
class PhraseTrigger(Trigger):
    """
    Abstract class for notifying user based on presence of certain string
    The constructor takes in 1 argument:
        phrase: string, one or more words separated by a single space
            will be split into its individual words, not case sensitive
    
    """
    def __init__(self, phrase):
        self.phrase = phrase.lower().split()
        
    def is_phrase_in(self, text):
        """

        text : string, to be searched for phrase
        
        returns True if each word in phrase appears exactly in text,
            words must be in order, seperated only by an arbitrary amount of
            whitespace or punctuation

        """
        word = ""
        words_matched = 0
        valid_seps = string.whitespace + string.punctuation
        for char in text:
            #if seperator, check the prior word for match
            #only check if you have a non-empty word, otherwise silently continue
            if char in valid_seps:
                if len(word) > 0:
                    #increment matched words count, reset word
                    if word.lower() == self.phrase[words_matched]:
                        words_matched += 1
                        #if all words matched, return true
                        if words_matched >= len(self.phrase):
                            return True
                    #no match means start over from 0 words matched
                    else:
                        words_matched = 0
                        #immediately check if this word is first word of phrase
                        if word.lower() == self.phrase[words_matched]:
                            words_matched += 1
                            #if all words matched, return true
                            if words_matched >= len(self.phrase):
                                return True
                    #reset word to empty before continuing
                    word = ""
            #if not a seperator, add to word
            else:
                word += char
        #Test final word for match
        if word.lower() == self.phrase[words_matched]:
            words_matched += 1
            #if all words matched, return true
            if words_matched >= len(self.phrase):
                return True
        #If reach here, no full match
        return False
    
                    

# Problem 3
class TitleTrigger(PhraseTrigger):
    """
    
    """
    def __init__(self, phrase):
        PhraseTrigger.__init__(self, phrase)
        
    def evaluate(self, story):
        if self.is_phrase_in(story.get_title()):
            return True
        return False

# Problem 4
class DescriptionTrigger(PhraseTrigger):
    """
    
    """
    def __init__(self, phrase):
        PhraseTrigger.__init__(self, phrase)
        
    def evaluate(self, story):
        if self.is_phrase_in(story.get_description()):
            return True
        return False

# TIME TRIGGERS

# Problem 5
class TimeTrigger(Trigger):
    """
    Constructor:
        Input: Time has to be in EST and in the format of "%d %b %Y %H:%M:%S".
        Convert time from string to a datetime before saving it as an attribute.
    """
    def __init__(self, time):
        self.time = datetime.strptime(time, "%d %b %Y %H:%M:%S")
# Problem 6
# TODO: BeforeTrigger and AfterTrigger
class BeforeTrigger(TimeTrigger):
    def __init__(self, time):
        TimeTrigger.__init__(self, time)
    
    def evaluate(self, story):
        if self.time > story.get_pubdate():
            return True
        return False

class AfterTrigger(TimeTrigger):
    def __init__(self, time):
        TimeTrigger.__init__(self, time)
    
    def evaluate(self, story):
        if self.time < story.get_pubdate():
            return True
        return False

# COMPOSITE TRIGGERS

# Problem 7
class NotTrigger(Trigger):
    def __init__(self, trigger):
        self.trigger = trigger
    def evaluate(self, story):
        return not self.trigger.evaluate(story)

# Problem 8
class AndTrigger(Trigger):
    def __init__(self, t1, t2):
        self.trigger1 = t1
        self.trigger2 = t2
        
    def evaluate(self, story):
        return self.trigger1.evaluate(story) and self.trigger2.evaluate(story)

# Problem 9
class OrTrigger(Trigger):
    def __init__(self, t1, t2):
        self.trigger1 = t1
        self.trigger2 = t2
        
    def evaluate(self, story):
        return self.trigger1.evaluate(story) or self.trigger2.evaluate(story)


#======================
# Filtering
#======================

# Problem 10
def filter_stories(stories, triggerlist):
    """
    Takes in a list of NewsStory instances.

    Returns: a list of only the stories for which a trigger in triggerlist fires.
    """
    matched_stories = []
    for story in stories:
        for trigger in triggerlist:
            if trigger.evaluate(story):
                matched_stories.append(story)
    return matched_stories



#======================
# User-Specified Triggers
#======================
# Problem 11
def read_trigger_config(filename):
    """
    filename: the name of a trigger configuration file

    Returns: a list of trigger objects specified by the trigger configuration
        file.
    """
    # We give you the code to read in the file and eliminate blank lines and
    # comments. You don't need to know how it works for now!
    trigger_file = open(filename, 'r')
    lines = []
    for line in trigger_file:
        line = line.rstrip()
        if not (len(line) == 0 or line.startswith('//')):
            lines.append(line)

    # line is the list of lines that you need to parse and for which you need
    # to build triggers
    trigger_dict = {}
    triggers_to_return = []
    for line in lines:
        line_words = line.split(",")
        # if line starts with ADD, add previously defined trigger to return list
        if line_words[0] == "ADD":
            for trigger_name in line_words[1:]:
                triggers_to_return.append(trigger_dict[trigger_name])
        #all other cases are trigger definitions
        elif line_words[1] == "TITLE":
            trigger_dict[line_words[0]] = TitleTrigger(line_words[2])
        elif line_words[1] == "DESCRIPTION":
            trigger_dict[line_words[0]] = DescriptionTrigger(line_words[2])
        elif line_words[1] == "AFTER":
            trigger_dict[line_words[0]] = AfterTrigger(line_words[2])
        elif line_words[1] == "BEFORE":
            trigger_dict[line_words[0]] = BeforeTrigger(line_words[2])
        elif line_words[1] == "NOT":
            trigger_dict[line_words[0]] = NotTrigger(trigger_dict[line_words[2]])
        elif line_words[1] == "AND":
            trigger_dict[line_words[0]] = AndTrigger(trigger_dict[line_words[2]], trigger_dict[line_words[3]])
        elif line_words[1] == "OR":
             trigger_dict[line_words[0]] = OrTrigger(trigger_dict[line_words[2]], trigger_dict[line_words[3]])
            
            

    print(lines) # for now, print it so you see what it contains!
    return triggers_to_return


SLEEPTIME = 120 #seconds -- how often we poll

def main_thread(master):
    # A sample trigger list - you might need to change the phrases to correspond
    # to what is currently in the news
    try:
        t1 = TitleTrigger("election")
        t2 = DescriptionTrigger("Trump")
        t3 = DescriptionTrigger("Clinton")
        t4 = AndTrigger(t2, t3)
        triggerlist = [t1, t4]

        # Problem 11
        # TODO: After implementing read_trigger_config, uncomment this line 
        triggerlist = read_trigger_config('triggers.txt')
        
        # HELPER CODE - you don't need to understand this!
        # Draws the popup window that displays the filtered stories
        # Retrieves and filters the stories from the RSS feeds
        frame = Frame(master)
        frame.pack(side=BOTTOM)
        scrollbar = Scrollbar(master)
        scrollbar.pack(side=RIGHT,fill=Y)

        t = "Google & Yahoo Top News"
        title = StringVar()
        title.set(t)
        ttl = Label(master, textvariable=title, font=("Helvetica", 18))
        ttl.pack(side=TOP)
        cont = Text(master, font=("Helvetica",14), yscrollcommand=scrollbar.set)
        cont.pack(side=BOTTOM)
        cont.tag_config("title", justify='center')
        button = Button(frame, text="Exit", command=root.destroy)
        button.pack(side=BOTTOM)
        guidShown = []
        def get_cont(newstory):
            if newstory.get_guid() not in guidShown:
                cont.insert(END, newstory.get_title()+"\n", "title")
                cont.insert(END, "\n---------------------------------------------------------------\n", "title")
                cont.insert(END, newstory.get_description())
                cont.insert(END, "\n*********************************************************************\n", "title")
                guidShown.append(newstory.get_guid())

        while True:

            print("Polling . . .", end=' ')
            # Get stories from Google's Top Stories RSS news feed
            stories = process("http://news.google.com/news?output=rss")

            # Get stories from Yahoo's Top Stories RSS news feed
            stories.extend(process("http://news.yahoo.com/rss/topstories"))

            stories = filter_stories(stories, triggerlist)

            list(map(get_cont, stories))
            scrollbar.config(command=cont.yview)


            print("Sleeping...")
            time.sleep(SLEEPTIME)

    except Exception as e:
        print(e)


if __name__ == '__main__':
    # root = Tk()
    # root.title("Some RSS parser")
    # t = threading.Thread(target=main_thread, args=(root,))
    # t.start()
    # root.mainloop()
    t1 = TitleTrigger("election")
    t2 = DescriptionTrigger("Trump")
    t3 = DescriptionTrigger("Clinton")
    t4 = AndTrigger(t2, t3)
    triggerlist = [t1, t4]

    # Problem 11

    triggerlist = read_trigger_config('triggers.txt')
    print(triggerlist)

