# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 17:06:03 2020

@author: purpl
"""

from ps5 import *
from datetime import timedelta

def test_NewsStory():
    pubdate = datetime.now()
    normal_news_story = \
        NewsStory("23", "Heartbreaking: Local Fool Forgets to Write Unit Tests",\
                 "A tale of hubris and chagrin.", "https://www.notreal.fake/site/",\
                     datetime.now())
    # print(normal_news_story.get_guid())
    # print(normal_news_story.get_title())
    # print(normal_news_story.get_description())
    # print(normal_news_story.get_link())
    # print(normal_news_story.get_pubdate())
    assert(normal_news_story.get_guid() == "23")
    assert(normal_news_story.get_title() == "Heartbreaking: Local Fool Forgets to Write Unit Tests")
    assert(normal_news_story.get_description() == "A tale of hubris and chagrin.")
    assert(normal_news_story.get_link() == "https://www.notreal.fake/site/")
    assert(normal_news_story.get_pubdate() == pubdate)
    
    empty_news_story = NewsStory("","","","",pubdate)
    # print(empty_news_story.get_guid())
    # print(empty_news_story.get_title())
    # print(empty_news_story.get_description())
    # print(empty_news_story.get_link())
    # print(empty_news_story.get_pubdate())
    assert(empty_news_story.get_guid() == "")
    assert(empty_news_story.get_title() == "")
    assert(empty_news_story.get_description() == "")
    assert(empty_news_story.get_link() == "")
    assert(empty_news_story.get_pubdate() == pubdate)
    
    none_news_story = NewsStory(None,None,None,None,None)
    # print(none_news_story.get_guid())
    # print(none_news_story.get_title())
    # print(none_news_story.get_description())
    # print(none_news_story.get_link())
    # print(none_news_story.get_pubdate())
    assert(none_news_story.get_guid() is None)
    assert(none_news_story.get_title() is None)
    assert(none_news_story.get_description() is None)
    assert(none_news_story.get_link() is None)
    assert(none_news_story.get_pubdate() is None)
    
    print("NewsStory OK")   

def test_PhraseTrigger():
    #These should all be equivalent
    test_trigger_1_lower = PhraseTrigger("purple lemon")
    test_trigger_1_capital = PhraseTrigger("Purple Lemon")
    test_trigger_1_upper = PhraseTrigger("PURPLE LEMON")
    test_triggers_1 = [test_trigger_1_lower, test_trigger_1_capital, test_trigger_1_upper]
    
    triggers_passed = 0
    for trigger in test_triggers_1:
        assert trigger.is_phrase_in("Purple Lemon")
        assert trigger.is_phrase_in("Purple Lemon Lemon")
        assert trigger.is_phrase_in("purple Purple Lemon Lemon")
        assert trigger.is_phrase_in("purple Purple no Lemon no  Purple    Lemon")
        assert trigger.is_phrase_in("             Purple Lemon")
        assert trigger.is_phrase_in("my usual username is Purple Lemon")
        assert trigger.is_phrase_in("my usual username is Purple     Lemon")
        assert trigger.is_phrase_in("my usual username is purple lemon")
        assert trigger.is_phrase_in("my usual username is Purple!@#$%^&*((Lemon")
        assert not trigger.is_phrase_in("Lemon Purple")
        assert not trigger.is_phrase_in("Purple Purple")
        assert not trigger.is_phrase_in("PurpleaaaaaaaLemon")
        assert not trigger.is_phrase_in("my usual username is PurpleLemon")
        assert not trigger.is_phrase_in("my usual username is Purple Lemon23")
        triggers_passed += 1
        #print("Triggers Passed:", triggers_passed)
    
    try: 
        test_trigger_1_lower.evaluate("this should fail")
        #If DOESN'T fail, reaches this line and assert false
        assert False
    except NotImplementedError:
        assert True
    
    print("PhraseTrigger OK")
    
def test_TimeTrigger():
    normal_time_trigger = TimeTrigger("3 Oct 2016 17:00:10")
    time_tuple = normal_time_trigger.time.timetuple()
    #print(time_tuple)
    assert time_tuple[0] == 2016
    assert time_tuple[1] == 10
    assert time_tuple[2] == 3
    assert time_tuple[3] == 17
    assert time_tuple[4] == 0
    assert time_tuple[5] == 10
    print("TimeTrigger OK")

if __name__ == '__main__':
    #Tests solution to part 1
    test_NewsStory()
    #Tests solution to part 2
    test_PhraseTrigger()
    #Defer to ps5_test.py for parts 3 and 4
    test_TimeTrigger()
    
