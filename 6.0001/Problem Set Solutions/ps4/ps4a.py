# Problem Set 4A
# Name: Lochlan Belford
# Collaborators:
# Time Spent: :30

def get_permutations(sequence):
    '''
    Enumerate all permutations of a given string

    sequence (string): an arbitrary string to permute. Assume that it is a
    non-empty string.  

    You MUST use recursion for this part. Non-recursive solutions will not be
    accepted.

    Returns: a list of all permutations of sequence

    Example:
    >>> get_permutations('abc')
    ['abc', 'acb', 'bac', 'bca', 'cab', 'cba']

    Note: depending on your implementation, you may return the permutations in
    a different order than what is listed here.
    '''
    
    #Base Case: sequence is 1 character long
    if len(sequence) <= 1:
        return [sequence]
    #Otherwise, recursively call get_permutations on sequence[1:]
    #then find all permutations of first character interpolated with rest
    else:
        first_char = sequence[0]
        first_char_permuatations = []
        all_prior_permuations = get_permutations(sequence[1:])
        for permutation in all_prior_permuations:
            for index in range(len(permutation) + 1):
                new_permutation = permutation[0:index] + first_char + permutation[index:]
                #check for duplicates before adding to list
                if new_permutation not in first_char_permuatations:
                    first_char_permuatations.append(new_permutation)
        return first_char_permuatations

if __name__ == '__main__':
    #EXAMPLE
    example_input = 'abc'
    print('Input:', example_input)
    print('Expected Output:', ['abc', 'acb', 'bac', 'bca', 'cab', 'cba'])
    print('Actual Output:', get_permutations(example_input))
    
#    # Put three example test cases here (for your sanity, limit your inputs
#    to be three characters or fewer as you will have n! permutations for a 
#    sequence of length n)
    test_input_0 = "a"
    print('Input:', test_input_0)
    print('Expected Output:', ['a'])
    print('Actual Output:', get_permutations(test_input_0))
    
    test_input_1 = "zzy"
    print('Input:', test_input_1)
    print('Expected Output:', ['yzz', 'zyz', 'zzy'])
    print('Actual Output:', get_permutations(test_input_1))
    
    test_input_2 = "aaa"
    print('Input:', test_input_2)
    print('Expected Output:', ['aaa'])
    print('Actual Output:', get_permutations(test_input_2))
    
    test_input_3 = "aBc"
    print('Input:', test_input_3)
    print('Expected Output:', ['aBc', 'acB', 'Bac', 'Bca', 'caB', 'cBa'])
    print('Actual Output:', get_permutations(test_input_3))
