# Problem Set 2, hangman.py
# Name: 
# Collaborators:
# Time spent:

# Hangman Game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)
import random
import string

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist



def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)
    
    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code

# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = load_words()


def is_word_guessed(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    for letter in secret_word:
        if letter not in letters_guessed:
            #If any letter in secret_word has not been guessed, not done yet
            return False
    #If made it through every letter successfully, word has been guessed!
    return True
    


def get_guessed_word(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    #PLACEHOLDER: the string used to mark unguessed letters
    PLACEHOLDER = "_ "
    #mixed_word: a string of letters and placeholders, based on user's guesses
    mixed_word = "" 
    for letter in secret_word:
        if letter in letters_guessed:
            mixed_word += letter
        else:
            mixed_word += PLACEHOLDER
    return mixed_word
            



def get_available_letters(letters_guessed):
    '''
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string (of letters), comprised of letters that represents which letters have not
      yet been guessed.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    available_letters = string.ascii_lowercase
    for letter in letters_guessed:
        available_letters = available_letters.replace(letter, "")
    return available_letters
    
    

def hangman(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses

    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Remember to make
      sure that the user puts in a letter!
    
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
    
    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    #game constants and variables
    DASH_LINE = "-------------" #used to separate guesses
    VOWELS = "aeiou"
    guesses_remaining = 6
    warnings_remaining = 3
    letters_guessed = []
    
    #Introduction
    print("Welcome to the game Hangman!")
    print("I am thinking of a word that is", len(secret_word), "letters long.")
    print("You have", warnings_remaining, "warnings left.")
    print(DASH_LINE)
    #Game Turn
    while guesses_remaining > 0 and not is_word_guessed(secret_word, letters_guessed):
        #Display status
        
        print("You have", guesses_remaining, "guesses left.")
        print("Available letters:", get_available_letters(letters_guessed))
        
        #prompt for next guess, auto-convert to lowercase
        guess = input("Please guess a letter: ").lower()
        guess_result_string = ""
        #check if guess is a letter
        if not guess.isalpha():
            guess_result_string += "Oops! That is not a valid letter."
            if warnings_remaining > 0:
                warnings_remaining -= 1
                guess_result_string += "You have " + str(warnings_remaining) + " warnings left"
            else:
                guesses_remaining -= 1
                guess_result_string += "You have no warnings left, so you lose one guess"
        #check if already guessed
        elif guess in letters_guessed:
            guess_result_string += "Oops! You've already guessed that letter. "
            if warnings_remaining > 0:
                warnings_remaining -= 1
                guess_result_string += "You have " + str(warnings_remaining) + " warnings left"
            else:
                guesses_remaining -= 1
                guess_result_string += "You have no warnings left so you lose one guess"
        #if valid guess and not repeat, check if correct, and record guess
        elif guess in secret_word:
            letters_guessed += guess
            guess_result_string += "Good guess"
        #otherwise record guess and subtract remaining guesses
        else:
            letters_guessed += guess
            if guess in VOWELS:
                guesses_remaining -= 2
            else:
                guesses_remaining -= 1
            guess_result_string += "Oops! That letter is not in my word"
        guess_result_string += ": " + get_guessed_word(secret_word, letters_guessed)
        print(guess_result_string)
        print(DASH_LINE)
    
    #Winning result
    if guesses_remaining > 0:
        print("Congratulations, you won!")
        #count unique letters in secret_word
        unique_letters = ''
        for letter in secret_word:
            if letter not in unique_letters:
                unique_letters += letter
        score = guesses_remaining * len(unique_letters)
        print("Your total score for this game is:", score)
    #losing result
    else:
        print("Sorry, you ran out of guesses. The word was", secret_word)
        
    



# When you've completed your hangman function, scroll down to the bottom
# of the file and uncomment the first two lines to test
#(hint: you might want to pick your own
# secret_word while you're doing your own testing)


# -----------------------------------



def match_with_gaps(my_word, other_word):
    '''
    my_word: string with _ characters, current guess of secret word
    other_word: string, regular English word
    returns: boolean, True if all the actual letters of my_word match the 
        corresponding letters of other_word, or the letter is the special symbol
        _ , and my_word and other_word are of the same length;
        False otherwise: 
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    #remove whitespace to make my_word its true length
    my_word = my_word.replace(" ", "")
    #can't be a match if different lengths
    if len(my_word) != len(other_word):
        return False
    wildcards = []
    invalid_wildcards = []
    for index in range(0, len(my_word)):
        #record all wildcard characters to compare at end
        if my_word[index] == "_":
            wildcards += other_word[index]
        #if a letter, must match exactly, and record to check against blanks at end
        elif my_word[index] == other_word[index]:
            invalid_wildcards += my_word[index]
        else:
            return False
    #Compare unmatched letters in other_word to letters already guessed
    for letter in wildcards:
        if letter in invalid_wildcards:
            return False
    #If function hasn't returned false by now, its a match!
    return True
            
        



def show_possible_matches(my_word):
    '''
    my_word: string with _ characters, current guess of secret word
    returns: nothing, but should print out every word in wordlist that matches my_word
             Keep in mind that in hangman when a letter is guessed, all the positions
             at which that letter occurs in the secret word are revealed.
             Therefore, the hidden letter(_ ) cannot be one of the letters in the word
             that has already been revealed.

    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    possible_matches = ''
    for word in wordlist:
        if match_with_gaps(my_word, word):
            possible_matches += word + " "
    if len(possible_matches) <= 0:
        print("No matches found")
    else:
        print(possible_matches)
        
            



def hangman_with_hints(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses
    
    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Make sure to check that the user guesses a letter
      
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
      
    * If the guess is the symbol *, print out all words in wordlist that
      matches the current guessed word. 
    
    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    DASH_LINE = "-------------" #used to separate guesses
    VOWELS = "aeiou"
    guesses_remaining = 6
    warnings_remaining = 3
    letters_guessed = []
    
    #Introduction
    print("Welcome to the game Hangman!")
    print("I am thinking of a word that is", len(secret_word), "letters long.")
    print("You have", warnings_remaining, "warnings left.")
    print(DASH_LINE)
    #Game Turn
    while guesses_remaining > 0 and not is_word_guessed(secret_word, letters_guessed):
        #Display status
        
        print("You have", guesses_remaining, "guesses left.")
        print("Available letters:", get_available_letters(letters_guessed))
        
        #prompt for next guess, auto-convert to lowercase
        guess = input("Please guess a letter: ").lower()
        guess_result_string = ""
        #first check if asking for a hint with *
        if guess == "*":
            print("Possible word matches are:")
            show_possible_matches(get_guessed_word(secret_word, letters_guessed))
        #check if guess is a letter
        else:
            if not guess.isalpha():
                guess_result_string += "Oops! That is not a valid letter."
                if warnings_remaining > 0:
                    warnings_remaining -= 1
                    guess_result_string += "You have " + str(warnings_remaining) + " warnings left"
                else:
                    guesses_remaining -= 1
                    guess_result_string += "You have no warnings left, so you lose one guess"
            #check if already guessed
            elif guess in letters_guessed:
                guess_result_string += "Oops! You've already guessed that letter. "
                if warnings_remaining > 0:
                    warnings_remaining -= 1
                    guess_result_string += "You have " + str(warnings_remaining) + " warnings left"
                else:
                    guesses_remaining -= 1
                    guess_result_string += "You have no warnings left so you lose one guess"
            #if valid guess and not repeat, check if correct, and record guess
            elif guess in secret_word:
                letters_guessed += guess
                guess_result_string += "Good guess"
            #otherwise record guess and subtract remaining guesses
            else:
                letters_guessed += guess
                if guess in VOWELS:
                    guesses_remaining -= 2
                else:
                    guesses_remaining -= 1
                guess_result_string += "Oops! That letter is not in my word"
            guess_result_string += ": " + get_guessed_word(secret_word, letters_guessed)
            print(guess_result_string)
        print(DASH_LINE)
    
    #Winning result
    if guesses_remaining > 0:
        print("Congratulations, you won!")
        #count unique letters in secret_word
        unique_letters = ''
        for letter in secret_word:
            if letter not in unique_letters:
                unique_letters += letter
        score = guesses_remaining * len(unique_letters)
        print("Your total score for this game is:", score)
    #losing result
    else:
        print("Sorry, you ran out of guesses. The word was", secret_word)



# When you've completed your hangman_with_hint function, comment the two similar
# lines above that were used to run the hangman function, and then uncomment
# these two lines and run this file to test!
# Hint: You might want to pick your own secret_word while you're testing.


if __name__ == "__main__":
     
    # To test part 2, comment out the pass line above and
    # uncomment the following two lines.
    
    #secret_word = choose_word(wordlist)
    #hangman(secret_word)

###############
    
    # To test part 3 re-comment out the above lines and 
    # uncomment the following two lines. 
    
    secret_word = choose_word(wordlist)
    hangman_with_hints(secret_word)
