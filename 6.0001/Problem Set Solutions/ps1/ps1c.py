# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 14:48:01 2020

@author: purpl
"""

#pre-defined variables
portion_down_payment = .25
total_cost = 1000000
global bisection_search_steps_taken
bisection_search_steps_taken = 0

#user-defined variables
annual_salary = float(input("Enter the starting salary: "))

#derived variables
target_savings = total_cost * portion_down_payment

def simulate_savings(savings_rate):
    '''
    Simulates 36 months of saving your monthly_salary at the given savings_rate,
    assuming no prior savings
    Parameters
    ----------
    savings_rate : float 
        assumed to be between 0.0 and 1.00 inclusive, representing 0% to 100%.

    Returns
    -------
    savings : float
        amount of money saved after 36 months of saving monthly_salary at savings_rate.

    '''
    semi_annual_raise = 0.07
    annual_return = 0.04
    savings = 0
    maximum_months = 36
    monthly_salary = annual_salary / 12
    monthly_return = annual_return / 12
    for months_passed in range(1, maximum_months + 1):
        #add a month of savings return and new portion of salary saved to current savings
        savings += savings * monthly_return
        savings += monthly_salary * savings_rate
        #after every 6th month, increase salary
        if months_passed % 6 == 0:
            monthly_salary += monthly_salary * semi_annual_raise
    return savings            

def bisection_search_savings_rate():
    '''
    Uses bisection search to find the lowest possible savings rate 
    for the entered annual_salary to reach the target_savings
    Returns
    -------
    float
        ideal savings rate between 0.0 and 1.0 inclusive to the fourth decimal
        (i.e. 0.4321 representing 43.21%).

    '''
    global bisection_search_steps_taken
    epsilon = 100 #acceptable margin of error for bisection search, in dollars
    low = 0
    high = 10000
    guess = (high + low) // 2
    result = simulate_savings(guess/10000.0)
    
    while abs(result - target_savings) >= epsilon:
        bisection_search_steps_taken += 1
        if result < target_savings:
            low = guess
        else:
            high = guess
        guess = (high + low) // 2
        result = simulate_savings(guess/10000.0)
    
    return (guess / 10000.0)


### MAIN CODE
#first make sure it is even possible to save enough in time allotted
max_possible_savings = simulate_savings(1.0)
if max_possible_savings < target_savings:
    print("It is not possible to pay the down payment in three years.")
else:
    ideal_savings_rate = bisection_search_savings_rate()
    print("Best savings rate:", ideal_savings_rate)
    print("Steps in bisection search:", bisection_search_steps_taken)
        