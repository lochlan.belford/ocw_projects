# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 13:49:43 2020

@author: purpl
"""


#pre-defined variables
portion_down_payment = 0.25
current_savings = 0
annual_return = 0.04

#user-defined variables
annual_salary = float(input("Enter your annual salary: "))
portion_saved = float(input("Enter the percent of your salary to save, as a decimal: "))
total_cost = float(input("Enter the cost of your dream home: "))

#derived variables
monthly_return = annual_return / 12
monthly_salary = annual_salary / 12
target_savings = total_cost * portion_down_payment

#Simulate savings and track months until target amount reached
months_until_down_payment = 0
while current_savings < target_savings:
    #increment number of months
    months_until_down_payment += 1
    #add a month of savings return and new portion of salary saved to current savings
    current_savings += current_savings * monthly_return
    current_savings += monthly_salary * portion_saved
    
#Number of months after exiting while loop is your answer
print("Number of months:", months_until_down_payment)