# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 14:38:26 2020

@author: purpl
"""

#pre-defined variables
portion_down_payment = 0.25
current_savings = 0
annual_return = 0.04

#user-defined variables
annual_salary = float(input("Enter your annual salary: "))
portion_saved = float(input("Enter the percent of your salary to save, as a decimal: "))
total_cost = float(input("Enter the cost of your dream home: "))
semi_annual_raise = float(input("Enter the semi-annual raise, as a decimal: "))

#derived variables
monthly_return = annual_return / 12
monthly_salary = annual_salary / 12
target_savings = total_cost * portion_down_payment

#Simulate savings and track months until target amount reached
months_passed = 0
while current_savings < target_savings:
    #increment number of months
    months_passed += 1
    #add a month of savings return and new portion of salary saved to current savings
    current_savings += current_savings * monthly_return
    current_savings += monthly_salary * portion_saved
    #after every 6th month, increase salary
    if months_passed % 6 == 0:
        monthly_salary += monthly_salary * semi_annual_raise
        
#Number of months after exiting while loop is your answer
months_until_down_payment = months_passed
print("Number of months:", months_until_down_payment)