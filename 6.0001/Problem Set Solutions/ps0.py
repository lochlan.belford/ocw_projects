# -*- coding: utf-8 -*-
"""
Created on Sun Jun  7 13:25:48 2020

@author: purpl
"""

import numpy

#Prompt user for input, cast to int
x = int(input('Enter number x: '))
y = int(input('Enter number y: '))

#calculate x**y and log2(x)
ans_power = x**y
ans_log = numpy.log2(x)

#print x^y and log2(x)
print('X**y = ' + str(ans_power))
print('log(x) = ' + str(ans_log))

# already playing around with a bunch of edge cases not accounted for in
# the problem--not expected here since it is a day 1 assignment, but things
# like extra newlines, 0 values, negative values... going to continue on for
# now but might be good to come back