# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 13:07:01 2020

@author: purpl
"""

def isIn(str1, str2):
    if str1 in str2 or str2 in str1:
        return True
    return False

print(isIn("ello", 'yellow'))
print(isIn("yellow", 'ello'))
print(isIn("blue", "yellow"))
