# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 13:28:59 2020

@author: purpl
"""


def fib(n):
    if n == 2:
        print("buzz")
    if n == 0 or n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)
    
fib(5)