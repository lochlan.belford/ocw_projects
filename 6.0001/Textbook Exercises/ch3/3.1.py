# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
# Precondition: User will enter a valid integer,
print("Welcome to ROOT FINDER v0.1")

#Ask user for an integer
x = int(input("Please enter an integer: "))

#runs faster for higher powers, so increment large to small
for pwr in range (5, 0, -1):
    root = 0
    if pwr == 1: #To prevent pwr == 1 running forever at large values, shortcut: root == x at pwr == 1
        root = x
    while root**pwr < abs(x):
        root += 1
    if root**pwr == abs(x):
        if x < 0: #For success on negative inputs with positive powers, root must be negative
            root = -root
        print("Found root:", root, "and pwr:", pwr, "where", str(root) + "**" + str(pwr) + "==" + str(x))
        break
    elif pwr <= 1: #While allowing pwr == 1, this should never be reached
        print ("No perfect root between 1 and 5 inclusive found for integer", x)
    