# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 11:47:21 2020

@author: purpl
"""

#Input: a string of comma separated decimals
s = str(input("Enter a string of decimals separated by commas, no spaces: "))

#Split string at commas and sum the decimals
list_of_decimals = s.split(',')

total = 0.0
for d in list_of_decimals:
    total += float(d)
    
#Output
print(total)