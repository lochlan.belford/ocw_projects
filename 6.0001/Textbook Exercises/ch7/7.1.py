# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 17:16:13 2020

@author: purpl
"""

def sumDigits(s):
    """
    
    Assumes s is a string
    Returns the sum of the decimal digits in s
        For example, if s is 'a2b3c' it returns 5
        
    """
    total = 0
    for char in s:
        try:
            total += int(char)
        except ValueError:
            print("not an int")
        except:
            print("some other error")
    return total


print(sumDigits('3'))
print(sumDigits('32'))
print(sumDigits('3a2'))
print(sumDigits('aaavb'))
print(sumDigits('a22aa5v9b0'))
print(sumDigits('0'))
print(sumDigits('-=%$'))
print(sumDigits('-=%$4'))
print(sumDigits(''))
