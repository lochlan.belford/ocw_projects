# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 17:26:35 2020

@author: purpl
"""


def findAnEven(l):
    """
    
    Assumes l is a list of integers
    Returns the first even number in l
    Raises ValueError if l does not contain an even number
    
    """
    for number in l:
        if number % 2 == 0:
            return number
    #If reach here, no evens, raise ValueError
    raise ValueError('findAnEven called on list with no evens')

print(findAnEven([2]))
print(findAnEven([1, 2]))
print(findAnEven([1, 3, 5, 2, 4]))
print(findAnEven([8, 1, 3, 5, 2, 4]))
print(findAnEven([0]))
#Uncomment following tests one at a time, each should raise an exception
#print(findAnEven([1]))
#print(findAnEven([]))
