# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 20:34:28 2020

@author: purpl
"""

#Multiple choice questions to check, my reasoning, verdict
#1-4: False-- "all types" dictionary keys must be hashable
"""
2-4: Comparing k unsorted searches to sorting and then k searches
        When is:   
            k * 0(n) >= k * 0(n*log(n))
            A: k = 1, 0(n) >= 0(n*log(n)) FALSE
            B: k = log(n), log(n) * 0(n) >= log(n) * 0(n * log(n)) TRUE, both nlogn
    Verdict: Right for wrong reasons, actually k * 0(log(n)) PLUS 0(nlogn) on right

3-4a-c: False-- when n = 1 reaches that line, ALL are sorted, inverse relationship
    Or is it trivially true *because* all are sorted?
    No again because of inverse relationship
    
3-4e: This is merge sort with a lot of extra copying in the merge routine
    I think that brings it up to n**2, copying n sublists multiple times
    Believe the final total is n [sorting] * log(n) [splitting] * n [merging] = n**2log(n)
    But is that it's own class? answerig n**2, will check after
"""

#Multiple choice total possible: 5 + 5 + 20 = 30
#Recieved:  5 + 5 + (15 to 20) = (25-30)

"""
4-1
L: non-empty list of integers
n: int in range (0, len(L)]
returns a list all contiguous sublists of exactly length = n from L
"""
def getSublists(L, n):
    sublists = []
    for starting_index in range(0, len(L) - (n-1)):
        sublists.append(L[starting_index:starting_index + n])
    return sublists
    
"""
4-2
L: non-empty list of integers
returns largest monotonically increasing subset within L
###Dumb mistake would have caught in testing--return subset LENGTH, not actual set
"""
def longestRun(L):
    #Find largest subsets first, first monotonic one is largest
    #Horrible worst case performance yeah?
    for subset_length in range(len(L), 0, -1):
        subsets_to_test = getSublists(L, subset_length)
        #Test each for monotonicity
        for subset in subsets_to_test:
            #Assume it is true
            monotonic = True
            for index in range(0, len(subset) - 1):
                if not subset[index] <= subset[index+1]:
                    #Mark as false and break
                    monotonic = False
                    break
            #If reach here still true, return answer
            #Worst case returns [L[0]]
            if monotonic:
                return subset_length
    
#Problem 5
## DO NOT MODIFY THE IMPLEMENTATION OF THE Person CLASS ##
class Person(object):
    def __init__(self, name):
        #create a person with name name
        self.name = name
        try:
            firstBlank = name.rindex(' ')
            self.lastName = name[firstBlank+1:]
        except:
            self.lastName = name
        self.age = None
    def getLastName(self):
        #return self's last name
        return self.lastName
    def setAge(self, age):
        #assumes age is an int greater than 0
        #sets self's age to age (in years)
        self.age = age
    def getAge(self):
        #assumes that self's age has been set
        #returns self's current age in years
        if self.age == None:
            raise ValueError
        return self.age
    def __lt__(self, other):
        #return True if self's name is lexicographically less
        #than other's name, and False otherwise
        if self.lastName == other.lastName:
            return self.name < other.name
        return self.lastName < other.lastName
    def __str__(self):
        #return self's name
        return self.name
        
class USResident(Person):
    """ 
    A Person who resides in the US.
    """
    def __init__(self, name, status):
        """ 
        Initializes a Person object. A USResident object inherits 
        from Person and has one additional attribute:
        status: a string, one of "citizen", "legal_resident", "illegal_resident"
        Raises a ValueError if status is not one of those 3 strings
        """
        Person.__init__(self, name)
        valid_statuses = ("citizen", "legal_resident", "illegal_resident")
        if status in valid_statuses:
            self.status = status
        else:
            raise ValueError
        
        
    def getStatus(self):
        """
        Returns the status
        """
        return self.status
    
"""
6-1
implement insert function that creates and maintains a doubly-linked list of Frobs
"""    
class Frob(object):
    def __init__(self, name):
        self.name = name
        self.before = None
        self.after = None
    def setBefore(self, before):
        # example: a.setBefore(b) sets b before a
        self.before = before
    def setAfter(self, after):
        # example: a.setAfter(b) sets b after a
        self.after = after
    def getBefore(self):
        return self.before
    def getAfter(self):
        return self.after
    def myName(self):
        return self.name
    
def insert(atMe, newFrob):
    """

    Parameters
    ----------
    atMe : Frob
        some preexisting Frob that may or may not already be in a list
    newFrob : Frob
        a new Frob with no connections, to be inserted into same list as atMe

    Returns
    -------
    None.

    """
    #Find out which side of atMe newFrob should go
    if newFrob.myName() < atMe.myName(): #Before
        #compare to next earliest Frob alphabetically
        frob_after_new = atMe
        next_earliest_frob = atMe.getBefore()
        #keep going until one is less than new frob OR reached start of list
        while (not next_earliest_frob is None) and \
                (newFrob.myName() < next_earliest_frob.myName()):
            frob_after_new = next_earliest_frob
            next_earliest_frob = next_earliest_frob.getBefore()
        #found correct place in list, link to both neigbors
        frob_after_new.setBefore(newFrob)
        newFrob.setAfter(frob_after_new)
        if not next_earliest_frob is None:
            newFrob.setBefore(next_earliest_frob)
            next_earliest_frob.setAfter(newFrob)
            
    else: #After
        #compare to next Frob alphabetically
        frob_before_new = atMe
        next_frob = atMe.getAfter()
        #keep going until one is greater than new frob OR reached end of list
        while (not next_frob is None) and \
                (newFrob.myName() > next_frob.myName()):
            frob_before_new = next_frob
            next_frob = next_frob.getAfter()
        #found correct place in list, link to both neigbors
        frob_before_new.setAfter(newFrob)
        newFrob.setBefore(frob_before_new)
        if not next_frob is None:
            newFrob.setAfter(next_frob)
            next_frob.setBefore(newFrob)
    
    
"""
6-2
Implement a recursive function findFront that finds the 
beginning of the linked list given any arbitrary Frob 
"""
def findFront(start):
    """
    
    Parameters
    ----------
    start : Frob
        a Frob that may or may not be in a doubly-linked list

    Returns
    -------
    Frob
        the first Frob in the same list as start
    
    """
    #Base case, we are already at the front
    if start.getBefore() is None:
        return start
    #otherwise, move one back in list and try again recursively
    else:
        return findFront(start.getBefore())

  
#Test Problem 4
test_list_sorted = [1,2,3,4,5,6,7,8,9,10]
test_list_reverse = [10,9,8,7,6,5,4,3,2,1]
test_list_mixed = [8,4,6,10,3,5,1,9,7,2]

#test 4-1
sublists_10 = getSublists(test_list_sorted, 10)
sublists_7 = getSublists(test_list_sorted, 7)
sublists_4 = getSublists(test_list_sorted, 4)
sublists_1 = getSublists(test_list_sorted, 1)
# print(sublists_10)
# print(sublists_7)
# print(sublists_4)
# print(sublists_1)
assert sublists_10 == [[1,2,3,4,5,6,7,8,9,10]]

#test 4-2
# print(longestRun(test_list_sorted))
# print(longestRun(test_list_reverse))
# print(longestRun(test_list_mixed))
assert longestRun(test_list_sorted) == [1,2,3,4,5,6,7,8,9,10]
assert longestRun(test_list_reverse) == [10]
assert longestRun(test_list_mixed) == [4,6,10]

#Test 5
#Provided
a = USResident('Tim Beaver', 'citizen')
# print(a.getStatus())
assert a.getStatus() == "citizen"
try:
    b = USResident('Tim Horton', 'non-resident')
    assert False #only gets here if not raising appropriate error
except ValueError:
    assert True
    
test_resident = USResident('John Smith', 'legal_resident')
assert test_resident.getStatus() == "legal_resident"
assert test_resident.getLastName() == "Smith"
test_illegal = USResident('Crime Mann', 'illegal_resident')
assert test_illegal.getStatus() == "illegal_resident"
assert test_illegal.getLastName() == "Mann"
assert test_illegal < test_resident
test_no_name = USResident('', 'citizen')
# print(a)
# print(test_resident)
# print(test_illegal)
# print(test_no_name)
assert test_no_name.getStatus() == "citizen"
assert test_no_name.getLastName() == ''
try:
    no_status = USResident('Typo Mann', '')
    assert False
except ValueError:
    assert True

#Test 6-1
#Provided
eric = Frob('eric')
andrew = Frob('andrew')
ruth = Frob('ruth')
fred = Frob('fred')
martha = Frob('martha')



insert(eric, fred)
insert(eric, ruth)
insert(ruth, Frob("zane"))
insert(fred, Frob("zane"))
insert(eric, andrew)
insert(ruth, martha)

next_frob = andrew
while not next_frob is None:
    print(next_frob.myName() +  ", then")
    next_frob = next_frob.getAfter()
print("No one!")

assert findFront(eric) == andrew
assert findFront(andrew) == andrew
assert findFront(ruth) == andrew
assert findFront(martha) == andrew
assert findFront(fred) == andrew

#Points possible coding problems: 10 + 10 + 10 + 10 + 10 = 50
#Points recieved coding problems: 10 + 10 + 10 + 10 + 10 = 50!
#total possible = 30 + 50 = 80
#final score = (25 to 30) + 50 = (75 to 80) 
#final grade = 93.75% to 100%!
