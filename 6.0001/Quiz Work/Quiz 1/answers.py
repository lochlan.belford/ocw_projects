# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 13:27:41 2020

@author: purpl
"""


#Answers to check at end with their concepts: 
#2-4: break statements, yes always terminate nearest nested loop
#3-2: function name capitalization, yest they can be capitalized but shouldn't
#

#Code answers
#4-1 --PASSED
def evalQuadratic(a, b, c, x):
    """
    a, b, c: numerical values for the coefficieents of a quadratic equation
    x: numerical value at which to evaluate the quadratic
    
    """
    return a * (x**2) + b * x + c

#4-2 -- Didn't read carefully enough, needs to PRINT, not return
def twoQuadratics(a1, b1, c1, x1, a2, b2, c2, x2):
    "uses evalQuadratic to sum the values of two quadratic equations at specific values"
    return evalQuadratic(a1, b1, c1, x1) + evalQuadratic(a2, b2, c2, x2)

#5 --PASSED, but not sure if //2 + 1 is actually helpful
def primesList(N):
    """
    N: an integer
    Returns a list of prime numbers between 2 and N
    """
    primes = []
    for number in range(2, N+1): #between 2 and N inclusive
        isPrime = True #Assume number is prime, then try to prove it isn't
        for divisor in range (2, number // 2 + 1): #Test up to number divided by half(?)
            if number % divisor == 0: #if divisible, not prime, end loop
                isPrime = False
                break
        if isPrime: #if reaches here still true, append to list of primes
            primes.append(number)
    return primes

#6 --PASSED
def count7(N):
    """
    
    N: a non-negative integer
    uses recursion to count the number of times 7 occurs in the digits of N
    
    """
    
    #Base case: N is one digit, aka less than 10
    if N < 10:
        # if the digit is 7, return 1 to add to total
        if N == 7:
            return 1
        # in all other single-digit cases return 0
        return 0
    
    #Iterations: Break into sub problems of rightmost digit, and remaining digits
    return count7(N // 10) + count7(N % 10)

#7 --PASSED
def uniqueValues(aDict):
    """
    aDict: a dictionary
    returns a list of keys that map to integer values that occur only once in aDict
    """
    uniques = [] #list to return, tracks unique values
    uniqueKeys = [] #keys referring to unique values, indexes correspond to value index in uniques
    duplicates = [] #list to prevent re-adding duplicate values after removed from uniques
    for key in aDict: #for each value
        v = aDict[key]
        if type(v) == int: #only consider integers
            if v not in uniques and v not in duplicates: #new unique found
                uniques.append(v)
                uniqueKeys.append(key)
            elif v in uniques: #mark as duplicate and remove from uniques
                duplicates.append(v)
                index_for_deletion= uniques.index(v)
                uniques.pop(index_for_deletion)
                uniqueKeys.pop(index_for_deletion)
            #only other case is already in duplicates, do nothing
    uniqueKeys.sort()
    return uniqueKeys
    
#8 --PARTIAL 12.4 out of 20.0, poor use of pop
# def satisfiesF(L):
#     """
#     Assumes L is a list of strings
#     Assume function f is already defined for you and maps a string to a Boolean
#     Mutates L such that it contains all of the strings, s, originally in L
#         such that f(s) returns True, and no other elements. Remaining elements
#         in L should be in the same order
#     Returns the length of L after mutation
        
#     """
#     indexes_marked_for_deletion = [] #keep track and do after loop, so not mutating while iterating
#     for i in range(0, len(L)):
#         if not f(L[i]): #record falses for deletion
#             indexes_marked_for_deletion.append(i)
#     for j in indexes_marked_for_deletion:
#         L.pop(j)
#     return len(L)

#8 fixed -- I am weak on when to use try-catch as flow control
def satisfiesF(L):
    """
    Assumes L is a list of strings
    Assume function f is already defined for you and maps a string to a Boolean
    Mutates L such that it contains all of the strings, s, originally in L
        such that f(s) returns True, and no other elements. Remaining elements
        in L should be in the same order
    Returns the length of L after mutation
        
    """
    mark_string = 'delete me' #string used to mark Falses for deletion
    for i in range(0, len(L)):
        if not f(L[i]):
            L[i] = mark_string
    while True: #I hate this
        try:
            L.remove(mark_string)
        except ValueError: #gone out of bounds, end
            break
    return len(L)

def f(s):
    #Returns true if there is an 'a' in string s
    return 'a' in s
    

#4 test
#print(twoQuadratics(5.69, 0.47, -3.68, -5.34, 4.93, 9.19, -2.27, -9.89))
    
#5 test
#print(primesList(1000))
    
#6 test
# print(count7(0))
# print(count7(8))
# print(count7(7))
# print(count7(10))
# print(count7(17))
# print(count7(70))
# print(count7(77))
# print(count7(717))
# print(count7(1237123))
# print(count7(7000000))
# print(count7(8989))
    
#7 test
# dictA = {'d': 1, 'a': 2, 'b': 0, 'c': -1}
# dictB = {'a': 1, 'b': 2, 'c': 1, 'd': -1}
# dictC = {'a': 2, 'b': 2, 'c': 2, 'd': -1}
# dictD = {'a': 2, 'b': 2, 'c': 2, 'd': 2}
# dictE = {'a': 'a', 'b': [2], 'c': 3.5, 'd': (2, 1), 'e': {'f': 1}}
# dictF = {1: 3, 2: 2, 3: 4, 5: -1}
# dictG = {('a', 'b'): 2, 'a': 'a', 'b': [2], 'c': 3.5, 'd': (2, 1), 'e': {'f': 1}}

# print(uniqueValues({}))
# print(uniqueValues({1: 2}))
# print(uniqueValues(dictA))
# print(uniqueValues(dictB))
# print(uniqueValues(dictC))
# print(uniqueValues(dictD))
# print(uniqueValues(dictE))
# print(uniqueValues(dictF))
# print(uniqueValues(dictG))
    
#8 test
# L = ['a', 'b', 'a']
# print(satisfiesF(L))
# print(L)
    
#Don't know how to grade #3, so final score is between 85% and 91%